library(Makurhini)

instalar_paquetes(c("igraph", "purrr", "raster", "sf", "magrittr", "future", "furrr", 
                    "formattable", "terra", "rmapshaper", "rgeos", "gdistance", 
                    "graph4lg", "sp"))

AP <- st_read(paste0("gpkg_temps/ap_temp5_",unidad_analisis,".gpkg"), quiet = TRUE)
Ecorregiones <- read_sf("conectividad/EcoregIII_shape/EcoregIII.shp", options = "ENCODING=UTF-8")

j <- "IDNUEVO"#Genero un nuevo ID le puse ese nombre que coincide con el objeto i, al final lo elimino
AP[[j]] <- 1:nrow(AP) 

AP_proj <- st_transform(AP, st_crs(Ecorregiones))
AP_proj <- TopoClean(AP_proj)

### Filtrar AP 100% Marinas o que no se superponen con Ecorregiones (i.e., AP que estan sobre islas)
AP_proj_ProtConn <- AP_proj[which(AP_proj$S_MARIN <= 100 |
                                    is.na(AP_proj$S_MARIN)),]
AP_proj_ProtConn <- over_poly(AP_proj_ProtConn, ms_dissolve(Ecorregiones), geometry = TRUE)

### 3) Cargar Resistencia = Mexbio SVI INEGI, se encuentra en el DRIVE: 2_GC_AMECID > modulo_conectividad
resistencia <- raster("conectividad/Indice_Impacto_Humano_2014.tif")
crs(resistencia) <- crs(as(Ecorregiones, "Spatial"))
### Reescalar
resistencia <- resistencia *100

###ProtConn
protconn.1 <- lapply(1:nrow(Ecorregiones), function(x){
  x.1 <- Ecorregiones[x,]
  
  d.1 <- crop(resistencia, 
              st_bbox(x.1)%>% st_as_sfc()%>% st_as_sf())%>%  mask(., x.1) 
  d.1 <- mean(raster::getValues(d.1), na.rm = TRUE) * 10000
  
  x.2 <- tryCatch(MK_ProtConn(nodes = AP_proj_ProtConn["IDNUEVO"], 
                              region = x.1,
                              area_unit = "ha",
                              distance = list(type= "least-cost", resistance = resistencia),
                              distance_thresholds = d.1,
                              probability = 0.5, transboundary = 50000,
                              LA = NULL, plot = FALSE, parallel = NULL,
                              delta = TRUE,
                              protconn_bound = FALSE,
                              write = NULL, intern = FALSE), error = function(err)err)
  if(inherits(x.2, "error")){
    x.2 <- NULL
  }
  return(x.2)
})


Tab1_ProtConn_Ecorreg <- purrr::map_dfr(1:nrow(Ecorregiones), function(x){
  x.0 <- Ecorregiones[x,]
  if(is.null(protconn.1[[x]][[1]])){
    x.2 <- data.frame("id" = x, 
                      "Nombre" = x.0$DESECON3,
                      "Protegido" = 0,
                      "Protegido y conectado" = 0,
                      "No ptotegido" = 0, 
                      check.names = FALSE)  
  } else {
    x.1 <- protconn.1[[x]][[1]] %>% as.data.frame(.)
    x.2 <- data.frame("id" = x, 
                      "Nombre" =  x.0$DESECON3,
                      "Protegido" = x.1[x.1[,3] == "Prot",4],
                      "Protegido y conectado" = x.1[x.1[,3] == "ProtConn",4],
                      "No ptotegido" = x.1[x.1[,3] == "Unprotected",4], 
                      check.names = FALSE)  
  }
  return(x.2) })
Ecorregiones_ProtConn <- cbind(Ecorregiones, Tab1_ProtConn_Ecorreg[,3:5])

### Tabla 2. Contribucion de cada AP a la conectividad (valor de ProtConn) de la ecorregion
### Seleccionar para cada AP su Ecorregion, es decir,
### aquella con la que tiene un mayor traslape
Select_AP_Ecorregion <- purrr::map_dfc(1:nrow(Ecorregiones), function(x){
  x.0 <- data.frame(x = rep(0, nrow(AP_proj_ProtConn)))
  x.1 <- over_poly(AP_proj_ProtConn, Ecorregiones[x,], geometry = TRUE)
  x.2 <- ms_clip(x.1, Ecorregiones[x,])
  parea <- as.numeric(st_area(x.2)*100/st_area(Ecorregiones[x,]))
  x.0[AP_proj_ProtConn[[j]] %in% x.2[[j]],] <- parea
  names(x.0) <- x
  return(x.0)
})
Select_AP_Ecorregion <- apply(Select_AP_Ecorregion, 1, which.max)
Tab2_ProtConn_Ecorreg_AP <- map_dfr(AP[[j]], function(x){
  if(x %in% AP_proj_ProtConn[[j]]){
    x.1 <- Select_AP_Ecorregion[which(AP_proj_ProtConn[[j]] == x)]
    x.2 <- protconn.1[[x.1]][[2]]
    
    if(is.null(x.2)){
      x.3 <- data.frame(j = x,
                        Ecorregion = "NA",
                        dProtegido = NA,
                        dProtConn = NA)  
    } else {
      x.3 <- data.frame(j = x, #Nombre del campo para el merge final
                        Ecorregion = Ecorregiones$DESECON3[x.1],
                        dProtegido = x.2$dProt[x.2[[j]] == x],
                        dProtConn = x.2$dProtConn[x.2[[j]] == x])  
    }
    
  } else {
    x.3 <- data.frame(j = x,
                      Ecorregion = "NA",
                      dProtegido = NA,
                      dProtConn = NA)
  }
  names(x.3)[1] <- j
  return(x.3)
})

AP$ECORREGION <- Tab2_ProtConn_Ecorreg_AP$Ecorregion
AP$dProtConn <- Tab2_ProtConn_Ecorreg_AP$dProtConn
AP$ECORREGION[AP$ECORREGION == "NA"] <- ""
AP$dProtConn[is.na(AP$dProtConn)] <- ""

st_write(AP,paste0("gpkg_temps/ap_temp6_",unidad_analisis,".gpkg"), 
         layer_options = "OVERWRITE=true",
         delete_dsn=TRUE) 